# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import datetime
class Dataclean():
    def __init__(self):
        self.original_ratios = []
        self.time_list =[]
        self.company_dict = {}
        self.datadict = {}
        self.feature_list = []

    def GuruTradinghistory(self):
        Guru = pd.read_csv("stockpick_new.csv")
        row = Guru.shape[0]
        column = Guru.shape[1]
        Guru = Guru.drop(labels = 'Impact', axis = 1)
        Guru = Guru.drop(labels = 'Price range', axis = 1)
        Guru = Guru.drop(labels='Price change from Avg', axis=1)
        Guru = Guru.drop(labels='current shares', axis=1)

        Guru['year'] = None
        Guru['month'] = None
        Guru['action index'] = None
        Guru['year&month'] = None
        Guru['year&season'] = None

        # there is two types of year&week index
        # 'year&week1' is consider the action season : 200203 -> convert to 200215 directly
        # 'year&week2' is consider the season before the action season : 200203 -> 200112 --> convert to 200152 week

        Guru['year&week1'] = None
        Guru['year&week2'] = None


        # a very important issue with pandas chain operation, Warning type SettingWithCopyWarning:
        for i in range(row):
            Guru.loc[i,'Firm ID'] = str(Guru.loc[i,'Firm ID']) + " US Equity"
            date =str(Guru.iloc[i]['Date'])
            year = date[0:4]
            month = date[4:6]
            action = Guru.loc[i,'Action']
            Guru.loc[i,'year'] = year

            if month == "03":
                Guru.loc[i, 'month'] = '03'
                season = '01'
                month_1 = '12'
                year_1 = str(int(year)-1)
            elif month == "06":
                Guru.loc[i, 'month'] = '06'
                season = '02'
                month_1 = '03'
                year_1 = year
            elif month == "09":
                Guru.loc[i, 'month'] = '09'
                season = '03'
                month_1 = '06'
                year_1 = year
            else :
                Guru.loc[i, 'month'] = '12'
                season = '04'
                month_1 = '09'
                year_1 = year

            Guru.loc[i,'year&month'] = year + str(Guru.loc[i,'month'])
            Guru.loc[i,'year&season'] = year + season

            newyear, newweek = self.return_yweek(datetime.date(int(year), int(month), 1))
            finalstring = str(newyear) + str(newweek).zfill(2)
            Guru.loc[i, 'year&week1'] = finalstring

            newyear_1,newweek_1 = self.return_yweek(datetime.date(int(year_1), int(month_1), 1))
            finalstring = str(newyear_1) + str(newweek_1).zfill(2)
            Guru.loc[i, 'year&week2'] = finalstring

            if action == "Sold Out" or action == "Reduce" :
                Guru.loc[i,'action index'] = 0

            elif action == "New Buy" or action == "Add":
                Guru.loc[i, 'action index'] = 1

            else:
                Guru.loc[i, 'action index'] = 'NaN'

        Guru = Guru.sort_values(by=['Firm ID'])
        rows = Guru.shape[0]
        company_index = ''
        company_index_pre = ''
        counts = 1
        Guru['Firm Label ID'] = None
        for i in range(rows):
            company_index = Guru.loc[i, 'Firm ID']
            if company_index == company_index_pre:
                company_index = company_index + str(counts)
                counts = counts + 1
            else:
                company_index_pre = company_index
                counts = 1
            Guru.loc[i, 'Firm Label ID'] = company_index

        #self.writecsv(Guru,'Guru_new')

        return Guru
# step 1 select each feature as the last quater report
    def cleanratio(self):
        Rachel = pd.read_csv("new_ratio.csv")
        Rachel.set_index('Index')
        cleaned_chart = []
        for i in range(len(Rachel) - 1):
            pre1 = Rachel.loc[i, 'Global Company Key']
            pre2 = Rachel.loc[i, 'Fiscal quarter end']
            temp1 = Rachel.loc[i + 1, 'Global Company Key']
            temp2 = Rachel.loc[i + 1, 'Fiscal quarter end']
            if pre1 == temp1 and pre2 == temp2:
                pass
            else:
                obs = Rachel.loc[i:i].values[0]
                obs = obs.tolist()
                cleaned_chart.append(obs)
        for i in range (len(cleaned_chart)):
            fiscal_end_date = cleaned_chart[i][3].split('/')
            new_fiscal_end_date = str(fiscal_end_date[0])+str(fiscal_end_date[2])
            cleaned_chart[i][3] = new_fiscal_end_date

        table_title = list(Rachel.loc[0:0])
        test = pd.DataFrame(columns=table_title, data=cleaned_chart)
        #test.to_csv("cleaned ratio.csv", encoding='gbk')
        self.original_ratios = test
        return test
    # construct the time list

    def gettime(self):
        time_list = []
        counter = 0
        for i in range(80):
            year = str(1999 + counter)
            if i%4 == 0:
                month = '03'
            elif i%4 == 1:
                month = '06'
            elif i%4 == 2:
                month = '09'
            else:
                month ='12'
                counter = counter+1
            time = year + month
            time_list.append(str(time))
        self.time_list = time_list
        return time_list

    def getCompany_number_dict(self):
        guru = pd.read_csv('uniqueC.csv')
        number = guru['GVKEY'].tolist()
        company = guru['Company'].tolist()
        #guru.set_index('Index')
        company_number_dict = {}
        for i in range(len(company)):
            temp = company[i] + " US Equity"
            company_number_dict[number[i]] = temp
        self.company_dict = company_number_dict
        return company_number_dict

    # create feature tables
    def createFeatureTables(self):

        """
        :return:  a dict of feature, and feature name list
        the key is feature name
        the element is dataframe of the feature

        feature_name_list =
         ['Enterprise Value Multiple','P/E (Diluted, Excl. EI)', 'Price/Cash flow', 'Net Profit Margin',
         'Gross Profit Margin', 'Return on Assets', 'Return on Equity', 'After-tax Return on Invested Capital',
         'Gross Profit/Total Assets', 'Common Equity/Invested Capital', 'Long-term Debt/Invested Capital', 'Capitalization Ratio',
         'Interest/Average Total Debt', 'Total Debt/EBITDA', 'Short-Term Debt/Total Debt', 'Cash Flow/Total Debt',
         'Free Cash Flow/Operating Cash Flow', 'Total Debt/Total Assets', 'Total Debt/Capital', 'Total Debt/Equity',
          'After-tax Interest Coverage', 'Interest Coverage Ratio', 'Cash Ratio', 'Current Ratio',
          'Asset Turnover', 'Price/Book']
        Tech_feature_name_list = ['MonthlyBeta', 'MonthlyReturn', 'VolatilityWeekly', 'TradingVolumnWeekly']
        Compensation

        """
        feature_name_list = []
        company_number_dict = self.getCompany_number_dict()
        ratios = self.cleanratio()
        title_list = ratios.columns.values.tolist()
        company_list = ratios.iloc[:, 1].tolist()
        company_list = list(set(company_list))
        length_company = len(company_list)
        time_list = self.gettime()
        length_time = len(time_list)
        features_dict = {}

        for i in range(5, len(title_list)):
            variable_name = title_list[i]
            empty_array = np.ones((length_time,length_company)) * 1000000
            company_pre = ''
            i = -1
            for j in range(len(ratios)):
                company = str(ratios.loc[j, 'Global Company Key'])
                time = ratios.loc[j, 'Fiscal quarter end']
                # change the form of the time variable which is directly import from Xiaoqiao table
                # separate time
                year = time[2:6]
                month = time[0:2]
                time = year + month
                if company_pre == company:
                    pass
                else:
                    i = i + 1
                    empty_array[0, i] = company
                    company_pre = company
                try:
                    time_index = time_list.index(time)
                    empty_array[time_index,i] = ratios.loc[j, variable_name]
                except ValueError:
                    month = str(time[4:6])
                    if month == '10' or month == '11':
                        new_time = '09'
                        time = str(time[0:4])+ new_time
                    elif month == '01' or month == '02':
                        new_time = '12'
                        time =  str(int(time[0:4]) - 1)+ new_time
                    elif month == '04' or month == '05':
                        new_time = '03'
                        time = str(time[0:4])+ new_time
                    else:
                        new_time = '06'
                        time = str(time[0:4])+ new_time

                    time_index = time_list.index(time)
                    empty_array[time_index, i] = ratios.loc[j, variable_name]

            #print(time_list)
            df = pd.DataFrame(data=empty_array)
            df = df.replace(1000000, 'NaN')

            for i in range(len(company_list)):
                df.loc[0,i] = company_number_dict[str(int(df.loc[0,i]))]
            for i in range(len(time_list)):
                df.loc[i,'Time'] = str(time_list[i])
            df = df.set_index('Time')
            df = df.drop(labels='199906')
            df.columns = df.iloc[0]
            df = df.drop(labels='199903')

            #if '/' in variable_name:
                #variable_name = variable_name.replace('/', '  divided  ')

            variable_name = str(variable_name)
            feature_name_list.append(variable_name)
            features_dict[variable_name] = df

        self.datadict = features_dict

        return features_dict, feature_name_list

    def GetTechFeatures(self):
        Techfeatures_dict = {}
        Tech = ['MonthlyBeta', 'MonthlyReturn', 'VolatilityWeekly', 'TradingVolumnWeekly']
        for ele in Tech:
            input = ele + '.csv'
            temp = pd.read_csv(input)
            rows = temp.shape[0]
            # print(temp.columns.values.tolist())
            for i in range(rows):
                temp.loc[i, 'Time'] = str(temp.loc[i, 'Time'])
            temp.set_index('Time')
            Techfeatures_dict[ele] = temp

        return Techfeatures_dict, Tech

    def GetCompensation(self):
        temp = pd.read_csv('Compensation.csv')
        temp.set_index('Time')
        return temp


            # How to index and slice
        #print(MonthlyBeta.loc['200001':'200008']['AAPL US Equity'])

    def writecsv(self,dataframe,feature_name):
        df = dataframe
        variable_name = feature_name
        output = variable_name + ".csv"
        try:
            df.to_csv(output, encoding='gbk')
            print(variable_name + "  finish")
        except FileNotFoundError:
            if '/' in variable_name:
                output = variable_name.replace('/', '  divided  ') + ".csv"
                df.to_csv(output, encoding='gbk')
                print(variable_name + "  finish")
            else:
                print(variable_name + "encounter mistake")

    def return_yweek(self,any_day):
        next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
        lastday_thismonth = next_month - datetime.timedelta(days=next_month.day)
        return lastday_thismonth.isocalendar()[0], lastday_thismonth.isocalendar()[1]


if __name__ == "__main__":
    data = Dataclean()
    Guru = data.GuruTradinghistory()
    tech,techlist = data.GetTechFeatures()
    f1 = tech['MonthlyBeta']
    print(type(f1.loc[2,'Time']))



    #data = Dataclean()
    #Guru = data.GuruTradinghistory()
    #features_dict, features_name_list = data.createFeatureTables()
    #data.GetCompensation()
    #features, feature_list = data.getTechFeature()
    #dataclean.writecsv(Guru,'Guruchart')
    # Rachel_feature_dict, feature_name_list = dataclean.createFeatureTables()
    # print(Rachel_feature_dict[feature_name_list[2]])
    # techfeature = dataclean.getTechFeature()
    # print(techfeature['Compensation'])
    # print(len(dataclean.datadict))
    #Guru chart need a column week
    #print(Guru)





