﻿# -*- coding:utf-8 -*-
from tslearn.utils import to_time_series_dataset
from tslearn.clustering import GlobalAlignmentKernelKMeans
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from Data import Dataclean

class Timeseries():

    def __init__(self):
        pass
    def GetTimeSeries(self, feature ,Guru):
        """
        :param feature:  the dataframe extract from features_dict
        :param Guru: the stock_pick_new.csv of all trading period
        :return: all companies in Guru , time series
        """
        #feature = feature.sort_values(by =['Time'])
        rows = Guru.shape[0]
        errorset = []
        action_list = []
        time_series_list = []
        company_list =[]
        for i in range(rows):
            time = Guru.loc[i, 'year&month']
            year = time[0:4]
            month = time[4:6]
            if month == '03':
                t_5 = str(int(year) - 6) + '12'
                t = str(int(year) - 1) + '12'
            else:
                t_5 = str(int(year) - 5) + '0' + str(int(month) - 3)
                t = year + str(int(month) - 3)

            company = Guru.loc[i, 'Firm ID']
            company_action = Guru.loc[i, 'Firm Label ID']

            try:
                time_series = feature[company][t_5:t].tolist()
                time_series_list.append(time_series)
                action_list.append(Guru.loc[i, 'action index'])
                company_list.append(company_action)

            except KeyError:
                pass

        return time_series_list, action_list, company_list


    def GetTechTimeseris(self,feature_name,feature,Guru):
        """
        ['MonthlyBeta'：两年不包含决策季
        'MonthlyReturn',：两年不包含决策季
        'VolatilityWeekly',：20
        周包含决策季？ 'TradingVolumnWeekly'：20
        周包含决策季？]
        """

        #feature = feature.set_index('Time')
        rows = Guru.shape[0]
        action_list = []
        time_series_list = []
        company_list = []
        if feature_name == 'MonthlyBeta' or 'MonthlyReturn':
            for i in range(rows):
                time = Guru.loc[i, 'year&week2']
                year = time[0:4]
                week = time[4:6]
                t = time
                t_5 = str(int(year) - 2) + week
                company = Guru.loc[i, 'Firm ID']
                company1 = Guru.loc[i, 'Firm Label ID']
                try:
                    time_series = feature[company][t_5:t].tolist()
                    # time_series = feature[company][t_5:t]
                    time_series_list.append(time_series)
                    action_list.append(Guru.loc[i, 'action index'])
                    company_list.append(company1)

                except KeyError:
                    pass

        elif feature_name == 'VolatilityWeekly' or 'TradingVolumnWeekly':
            for i in range(rows):
                time = Guru.loc[i, 'year&week1']
                year = time[0:4]
                week = time[4:6]

                # take 20 weeks before
                temp = int(week) - 20
                if temp > 0:
                    t = time
                    t_5 = year + str(temp)
                else:
                    t = time
                    t_5 = str(int(year)-1) + str(72 - int(week))

                company = Guru.loc[i, 'Firm ID']
                company1 = Guru.loc[i, 'Firm Label ID']

                try:
                    time_series = feature[company][t_5:t].tolist()
                    # time_series = feature[company][t_5:t]
                    time_series_list.append(time_series)
                    action_list.append(Guru.loc[i, 'action index'])
                    company_list.append(company1)

                except KeyError:
                    pass

        else:
            pass

        return time_series_list, action_list, company_list

    def CleanNaN(self,temp,action,company_list):
        # deal with 'NAN' from beginning
        for j in range(len(temp)):
            ts = temp[j]
            if 'NaN' in ts or '' in ts or 0 in ts:
                for i in range(len(ts)):
                    if ts[i] == 'NaN' or ts[i] == 'nan' or ts[i] == 0:
                        ts[i] = 'Delete'
                    else:
                        break
            else:
                pass

        # deal with 'NAN' from end
        for j in range(len(temp)):
            ts = temp[j]
            if 'NaN' in ts or '' in ts or 0 in ts:
                for i in range(len(ts)):
                    k = len(ts) - i - 1
                    if ts[k] == 'NaN' or ts[k] == 'nan' or ts[k] == 0:
                        ts[k] = 'Delete'
                    else:
                        break
            else:
                pass

        for i in range(len(temp)):
            if 'Delete' in temp[i]:
                temp[i] = temp[i].remove('Delete')
            else:
                pass

        new_temp = []
        new_action = []
        new_company = []
        for i in range(len(temp)):
            if temp[i] == None:
                pass
            else:
                new_action.append(action[i])
                new_temp.append(temp[i])
                new_company.append(company_list[i])

        new_new_temp = []
        non_zero = 0
        for ele in new_temp:
            ts = []
            for i in range(len(ele)):
                if ele[i] == 'nan' or ele[i] == 'NaN':
                    ts.append(non_zero)
                else:
                    ts.append(ele[i])
                    non_zero = ele[i]
            new_new_temp.append(ts)

        return new_new_temp,new_action,new_company


    def KeepActionUnion(self,featurename):
        pass






if __name__ == "__main__":
    data = Dataclean()
    Guru = data.GuruTradinghistory()
    #features_dict, features_name_list = data.createFeatureTables()
    techfeatures_dict, techfeatures_name_list = data.GetTechFeatures()
    ts_generator = Timeseries()
    for ele in techfeatures_name_list :
        feature = techfeatures_dict[ele]
        feature = feature.set_index('Time')
        temp, action, company_list = ts_generator.GetTechTimeseris(ele,feature, Guru)
        print(company_list)
        #print(feature)
        #print(company_list)















